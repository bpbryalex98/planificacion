<?php

namespace App\Entity;

use App\Repository\PrediccionesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrediccionesRepository::class)
 */
class Predicciones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $asignatura;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $campus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $departamento;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $regresion_lineal;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $suavizamiento_exponencial;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $arboles_decision;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAsignatura(): ?string
    {
        return $this->asignatura;
    }

    public function setAsignatura(string $asignatura): self
    {
        $this->asignatura = $asignatura;

        return $this;
    }

    public function getCampus(): ?string
    {
        return $this->campus;
    }

    public function setCampus(string $campus): self
    {
        $this->campus = $campus;

        return $this;
    }

    public function getDepartamento(): ?string
    {
        return $this->departamento;
    }

    public function setDepartamento(string $departamento): self
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function getRegresionLineal(): ?string
    {
        return $this->regresion_lineal;
    }

    public function setRegresionLineal(string $regresion_lineal): self
    {
        $this->regresion_lineal = $regresion_lineal;

        return $this;
    }

    public function getSuavizamientoExponencial(): ?string
    {
        return $this->suavizamiento_exponencial;
    }

    public function setSuavizamientoExponencial(string $suavizamiento_exponencial): self
    {
        $this->suavizamiento_exponencial = $suavizamiento_exponencial;

        return $this;
    }

    public function getArbolesDecision(): ?string
    {
        return $this->arboles_decision;
    }

    public function setArbolesDecision(string $arboles_decision): self
    {
        $this->arboles_decision = $arboles_decision;

        return $this;
    }

        /**
     * Convierte los atributos de la entidad en un array asociativo.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'asignatura' => $this->asignatura,
            'campus' => $this->campus,
            'departamento' => $this->departamento,
            'regresion_lineal' => $this->regresion_lineal,
            'suavizamiento_exponencial' => $this->suavizamiento_exponencial,
            'arboles_decision' => $this->arboles_decision,
        ];
    }


}
