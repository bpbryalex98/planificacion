<?php

namespace App\Entity;

use App\Repository\PaoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaoRepository::class)
 */
class Pao
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Departamento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $campus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $id_docente;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $docente;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $asignatura;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nrc;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_estudiantes;

    /**
     * @ORM\Column(type="time")
     */
    private $hora_entrada;

    /**
     * @ORM\Column(type="time")
     */
    private $hora_salida;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $lunes;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $martes;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $miercoles;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $jueves;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $viernes;

    /**
     * @ORM\Column(type="integer")
     */
    private $hora_dia;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_dias;

    /**
     * @ORM\Column(type="integer")
     */
    private $horas;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $periodo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDepartamento(): ?string
    {
        return $this->Departamento;
    }

    public function setDepartamento(string $Departamento): self
    {
        $this->Departamento = $Departamento;

        return $this;
    }

    public function getCampus(): ?string
    {
        return $this->campus;
    }

    public function setCampus(string $campus): self
    {
        $this->campus = $campus;

        return $this;
    }

    public function getIdDocente(): ?string
    {
        return $this->id_docente;
    }

    public function setIdDocente(string $id_docente): self
    {
        $this->id_docente = $id_docente;

        return $this;
    }

    public function getDocente(): ?string
    {
        return $this->docente;
    }

    public function setDocente(string $docente): self
    {
        $this->docente = $docente;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getAsignatura(): ?string
    {
        return $this->asignatura;
    }

    public function setAsignatura(string $asignatura): self
    {
        $this->asignatura = $asignatura;

        return $this;
    }

    public function getNrc(): ?string
    {
        return $this->nrc;
    }

    public function setNrc(string $nrc): self
    {
        $this->nrc = $nrc;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNumeroEstudiantes(): ?int
    {
        return $this->numero_estudiantes;
    }

    public function setNumeroEstudiantes(int $numero_estudiantes): self
    {
        $this->numero_estudiantes = $numero_estudiantes;

        return $this;
    }

    public function getHoraEntrada(): ?\DateTimeInterface
    {
        return $this->hora_entrada;
    }

    public function setHoraEntrada(\DateTimeInterface $hora_entrada): self
    {
        $this->hora_entrada = $hora_entrada;

        return $this;
    }

    public function getHoraSalida(): ?\DateTimeInterface
    {
        return $this->hora_salida;
    }

    public function setHoraSalida(\DateTimeInterface $hora_salida): self
    {
        $this->hora_salida = $hora_salida;

        return $this;
    }

    public function getLunes(): ?string
    {
        return $this->lunes;
    }

    public function setLunes(string $lunes): self
    {
        $this->lunes = $lunes;

        return $this;
    }

    public function getMartes(): ?string
    {
        return $this->martes;
    }

    public function setMartes(string $martes): self
    {
        $this->martes = $martes;

        return $this;
    }

    public function getMiercoles(): ?string
    {
        return $this->miercoles;
    }

    public function setMiercoles(string $miercoles): self
    {
        $this->miercoles = $miercoles;

        return $this;
    }

    public function getJueves(): ?string
    {
        return $this->jueves;
    }

    public function setJueves(string $jueves): self
    {
        $this->jueves = $jueves;

        return $this;
    }

    public function getViernes(): ?string
    {
        return $this->viernes;
    }

    public function setViernes(string $viernes): self
    {
        $this->viernes = $viernes;

        return $this;
    }

    public function getHoraDia(): ?int
    {
        return $this->hora_dia;
    }

    public function setHoraDia(int $hora_dia): self
    {
        $this->hora_dia = $hora_dia;

        return $this;
    }

    public function getNumeroDias(): ?int
    {
        return $this->numero_dias;
    }

    public function setNumeroDias(int $numero_dias): self
    {
        $this->numero_dias = $numero_dias;

        return $this;
    }

    public function getHoras(): ?int
    {
        return $this->horas;
    }

    public function setHoras(int $horas): self
    {
        $this->horas = $horas;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPeriodo(): ?string
    {
        return $this->periodo;
    }

    public function setPeriodo(string $periodo): self
    {
        $this->periodo = $periodo;

        return $this;
    }


    /**
     * Convierte los atributos de la entidad en un array asociativo.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'departamento' => $this->Departamento,
            'campus' => $this->campus,
            'id_docente' => $this->id_docente,
            'docente' => $this->docente,
            'codigo' => $this->codigo,
            'asignatura' => $this->asignatura,
            'nrc' => $this->nrc,
            'status' => $this->status,
            'numero_estudiantes' => $this->numero_estudiantes,
            'hora_entrada' => $this->hora_entrada->format('H:i:s'), // Formato de hora
            'hora_salida' => $this->hora_salida->format('H:i:s'), // Formato de hora
            'lunes' => $this->lunes,
            'martes' => $this->martes,
            'miercoles' => $this->miercoles,
            'jueves' => $this->jueves,
            'viernes' => $this->viernes,
            'hora_dia' => $this->hora_dia,
            'numero_dias' => $this->numero_dias,
            'horas' => $this->horas,
            'tipo' => $this->tipo,
            'periodo' => $this->periodo,
        ];
    }


}
