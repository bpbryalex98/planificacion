<?php

namespace App\Controller;

use App\Repository\PaoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaoController extends AbstractController
{

    private $paoRepository;    
    public function __construct(PaoRepository $paoRepository)
    {
        $this->paoRepository = $paoRepository;
    }

    /**
     * @Route("/pao", name="app_pao")
     */
    function index(Request $request): JsonResponse
    {
        $fullData = $request->query->get('full'); // Check if the 'full' parameter is set to 'true'
    
        if ($fullData === 'true') {
            $data = $this->paoRepository->findAll(); // Get all items
            $totalItems = count($data); // Total number of items
        } else {
            $page = $request->query->getInt('page', 1); // Get the 'page' parameter from the query string
    
            $itemsPerPage = 5000; // Number of items per page
            $offset = ($page - 1) * $itemsPerPage;
    
            $data = $this->paoRepository->findBy([], [], $itemsPerPage, $offset);
            $totalItems = $this->paoRepository->count([]); // Total number of items
        }
    
        $dataArray = [];
    
        foreach ($data as $item) {
            $dataArray[] = $item->toArray();
        }
    
        $responseData = [
            //'page' => $page ?? 1,
            //'totalItems' => $totalItems,
            'data' => $dataArray,
        ];
    
        $response = new JsonResponse($dataArray, 200); // The last parameter 'true' is for pretty-printing JSON
    
        return $response;
    }
    

}
